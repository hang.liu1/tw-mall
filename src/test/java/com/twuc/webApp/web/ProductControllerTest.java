package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.ProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductControllerTest extends APITestBase {

    @Autowired
    private ProductRepository productRepository;

    private String imageUrl = "https://i.ibb.co/z7sSVdy/cole.png";

    private MockHttpServletRequestBuilder createProductRequest(ProductRequest productRequest) throws JsonProcessingException {
        return post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        new ObjectMapper()
                                .writeValueAsString(productRequest));
    }


    private List<Product> createProducts(int count){
        return Stream.iterate(0, n -> n + 1)
                .limit(count)
                .map((n) -> new Product("name" + n, n, "unit" + n, imageUrl))
                .collect(Collectors.toList());
    }


    @Test
    void should_post_product() throws Exception {
        getMockMvc().perform(createProductRequest(new ProductRequest("name", 1, "unit", imageUrl)))
                .andExpect(status().is(201));

        assertTrue(productRepository.findProductByName("name").isPresent());

    }

    @Test
    void should_not_post_product_when_name_exist() throws Exception {
        getMockMvc().perform(createProductRequest(new ProductRequest("name", 1, "unit", imageUrl)))
                .andExpect(status().is(201));

        assertThrows(org.springframework.dao.DataIntegrityViolationException.class, () -> productRepository.saveAndFlush(new Product("name", 1, "unit", imageUrl)));

    }

    @Test
    void should_not_post_product_when_format_is_wrong() throws Exception {
        getMockMvc().perform(createProductRequest(new ProductRequest(null, 1, "unit", "aaa")))
                .andExpect(status().is(400));

        getMockMvc().perform(createProductRequest(new ProductRequest("name", 1, "unit", "aaa")))
                .andExpect(status().is(400));

    }

    @Test
    void should_get_all_product() throws Exception {
        List<Product> products = createProducts(5);
        flushAndClear(() -> {
            productRepository.saveAll(products);
        });

        getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name").value(products.get(0).getName()))
                .andExpect(jsonPath("$[4].price").value(products.get(4).getPrice()));


    }
}
