package com.twuc.webApp.web;

import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class JpaTest  {
    
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private EntityManager entityManager;

    private String imageUrl = "https://i.ibb.co/z7sSVdy/cole.png";


    private void flushAndClear(Runnable run){
        run.run();
        entityManager.flush();
        entityManager.clear();
    }

    private List<Product> createProducts(int count){
        return Stream.iterate(0, n -> n + 1)
                .limit(count)
                .map((n) -> new Product("name" + n, n, "unit" + n, imageUrl))
                .collect(Collectors.toList());
    }

    @Test
    void should_add_product() {
        flushAndClear(() -> {
            Product product = new Product("name", 1, "unit", imageUrl);
            productRepository.save(product);
        });

        assertTrue(productRepository.findProductByName("name").isPresent());
    }


    @Test
    void should_not_add_product_when_name_exist() {
        flushAndClear(() -> {
                Product product = new Product("name", 1, "unit", imageUrl);
                productRepository.save(product);
        });

        assertThrows(org.springframework.dao.DataIntegrityViolationException.class, () -> productRepository.saveAndFlush(new Product("name", 1, "unit", imageUrl)));

    }

    @Test
    void should_get_all_products() {
        flushAndClear(() -> {
            List<Product> products = createProducts(5);
            productRepository.saveAll(products);
        });

        assertEquals(5, productRepository.findAll().size());
    }
}
