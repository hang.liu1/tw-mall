package com.twuc.webApp.web;

import com.twuc.webApp.contract.ProductRequest;
import com.twuc.webApp.contract.ProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = "*")
public class ProductController {
    private ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @PostMapping
    public ResponseEntity postProduct(@RequestBody @Valid ProductRequest productRequest){
        Product product = new Product(productRequest.getName(), productRequest.getPrice(), productRequest.getUnit(), productRequest.getImageUrl());
        productRepository.saveAndFlush(product);

        return ResponseEntity.status(201).build();
    }

    @GetMapping
    public ResponseEntity<List<ProductResponse>> getAllProduct(){
        List<Product> products = productRepository.findAll();
        List<ProductResponse> productResponses = products
                .stream()
                .map(product -> new ProductResponse(product.getId(), product.getName(), product.getPrice(), product.getUnit(), product.getImageUrl()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(productResponses);
    }
}
