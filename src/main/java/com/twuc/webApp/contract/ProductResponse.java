package com.twuc.webApp.contract;

public class ProductResponse {
    private Long id;
    private String name;
    private Integer price;
    private String unit;
    private String imageUrl;

    public ProductResponse() {
    }

    public ProductResponse(Long id, String name, Integer price, String unit, String imageUrl) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
