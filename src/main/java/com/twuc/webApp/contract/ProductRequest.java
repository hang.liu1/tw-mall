package com.twuc.webApp.contract;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;

public class ProductRequest {
    @NotNull
    private String name;

    @NotNull
    private Integer price;

    @NotNull
    private String unit;

    @NotNull
    @URL
    private String imageUrl;

    public ProductRequest() {
    }

    public ProductRequest(String name, Integer price, String unit, String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
